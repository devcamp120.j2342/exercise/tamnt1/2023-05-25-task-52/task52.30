import java.util.ArrayList;

import model.Person;

public class App {
    public static void main(String[] args) throws Exception {
        ArrayList<Person> arrayList = new ArrayList<>();
        // Subtask2
        Person person0 = new Person("Tam", 20);
        Person person1 = new Person("John", 25, "70 kg");
        Person person2 = new Person("Alice", 30, "60 kg", 5000000);
        Person person3 = new Person("Mike", 35, "80 kg", 6000000, new String[] { "Dog", "Cat" });
        arrayList.add(person0);
        arrayList.add(person1);
        arrayList.add(person2);
        arrayList.add(person3);
        // in ra man hinh
        // Subtask3
        for (Person person : arrayList) {
            System.out.println(person);
        }
    }
}
