﻿package model;

import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Locale;

public class Person {
    private String name;
    private int age;
    private String weight;
    private long salary;
    private String[] pets;

    // Subtask1
    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Person(String name, int age, String weight) {
        this.name = name;
        this.age = age;
        this.weight = weight;
    }

    public Person(String name, int age, String weight, long salary) {
        this.name = name;
        this.age = age;
        this.weight = weight;
        this.salary = salary;
    }

    public Person(String name, int age, String weight, long salary, String[] pets) {
        this.name = name;
        this.age = age;
        this.weight = weight;
        this.salary = salary;
        this.pets = pets;
    }

    // Subtask4
    @Override
    public String toString() {
        NumberFormat currencyFormat = NumberFormat.getCurrencyInstance(Locale.forLanguageTag("vi-VN"));
        String formattedSalary = currencyFormat.format(salary);
        return "Person [name=" + name + ", age=" + age + ", weight=" + weight + ", salary=" + formattedSalary + ", pets="
                + Arrays.toString(pets) + "]";
    }
}
